use core::time::Duration;
use std::time::Instant;
use std::net::SocketAddr;
use binance_stream::MarketStream;

pub fn lookup(host: Option<&str>, service: Option<&str>, ret: &mut Vec<SocketAddr>) -> Result<(),u8>
{
    use core::ptr;
    use std::ffi::CString;
    use std::net;
    use libc::{AF_INET,AF_INET6,c_int,addrinfo};

    let hhh = host.map(|h| CString::new(h).unwrap() );
    let c_host = hhh.as_ref().map_or(ptr::null(), |s| s.as_ptr());

    let sss = service.map(|s| CString::new(s).unwrap() );
    let c_sss = sss.as_ref().map_or(ptr::null(), |s| s.as_ptr());

    let mut c_hints: addrinfo = unsafe { core::mem::zeroed() };
    c_hints.ai_flags = 0;
    c_hints.ai_family = 0;
    c_hints.ai_socktype = libc::SOCK_STREAM;
    c_hints.ai_protocol = 0;

    let mut res = ptr::null_mut();

    let status = unsafe { libc::getaddrinfo(c_host, c_sss, &mut c_hints, &mut res) };
    if status != 0 {  return Err(1);  }

    let mut p_addr_i = res;
    while !p_addr_i.is_null() {
        let addr_i = unsafe { *p_addr_i };
        match addr_i.ai_family {
            AF_INET => {
                let sa = unsafe { *(addr_i.ai_addr as *mut libc::sockaddr_in) };
                assert_eq!(sa.sin_family as c_int, AF_INET);
                let arr_addr = sa.sin_addr.s_addr.to_le_bytes();
                let sock_addr = SocketAddr::new(
                    net::IpAddr::V4(
                        net::Ipv4Addr::new(
                            arr_addr[0],arr_addr[1],
                            arr_addr[2],arr_addr[3])),
                    u16::from_be(sa.sin_port));
                ret.push(sock_addr);
            }
            AF_INET6 => {
                let sa = unsafe { *(addr_i.ai_addr as *mut libc::sockaddr_in6) };
                assert_eq!(sa.sin6_family as c_int, AF_INET6);
                let arr_addr = sa.sin6_addr.s6_addr;
                let sock_addr = SocketAddr::new(
                    net::IpAddr::V6(
                        net::Ipv6Addr::new(
                            u16::from_be_bytes([arr_addr[0],arr_addr[1]]),
                            u16::from_be_bytes([arr_addr[2],arr_addr[3]]),
                            u16::from_be_bytes([arr_addr[4],arr_addr[5]]),
                            u16::from_be_bytes([arr_addr[6],arr_addr[7]]),
                            u16::from_be_bytes([arr_addr[8],arr_addr[9]]),
                            u16::from_be_bytes([arr_addr[10],arr_addr[11]]),
                            u16::from_be_bytes([arr_addr[12],arr_addr[13]]),
                            u16::from_be_bytes([arr_addr[14],arr_addr[15]]),
                            )),
                    u16::from_be(sa.sin6_port));
                ret.push(sock_addr);
            }
            _ => { return Err(2); }
        }
        p_addr_i = addr_i.ai_next as *mut addrinfo;
    }

    unsafe { libc::freeaddrinfo(res); }

    Ok(())
}


pub fn lookup_adress(h: &str) -> Option<SocketAddr>
{
    let api_address;
    let mut addrs = Vec::with_capacity(8);
    match lookup(Some(h), Some("https"), &mut addrs) {
        Ok(_) => {
            if addrs.is_empty() {  return None;  }
            else {  api_address = addrs.first().unwrap().clone();  }
        }
        Err(e) => {
            eprintln!("  error: name resolving failed {:?}", e);
            return None;
        }
    }
    Some(api_address)
}



fn main() -> Result<(), u8>
{

    println!("=== BINANCE ===");

    println!("stream.binance.com");
    let address = lookup_adress("stream.binance.com").ok_or(3)?;
    println!("  {}", &address);

    let symbol = "ETHFDUSD";


    let mut trades = Vec::with_capacity(256);
    unsafe { trades.set_len(256); }

    println!("{}", symbol);

    let mut strm = MarketStream::new(address, symbol);
    strm.connect();
    let tm = Instant::now();
    while tm.elapsed() < Duration::from_secs(10) {
        match strm.take_trades(&mut trades) {
            Ok(c) => {
                for t in &trades[0..c] {
                    println!("{:?}", t);
                    println!("lat: {}ms", strm.latency());
                }
            }
            Err(e) => {
                dbg!(e);
                break;
            }
        }
        std::thread::sleep(Duration::from_millis(10));
    }

    Ok(())
}
