use core::mem;
use std::io;
use std::io::BufReader;
// use core::time::Duration;
use std::net::SocketAddr;
use socket2::{Socket,SockAddr};
use native_tls::{TlsConnector,MidHandshakeTlsStream};

fn sub_slice(data: &[u8], s: &[u8]) -> Option<usize>
{
    data.windows(s.len()).position(|win| win == s)
}

fn would_block(e: &io::Error) -> bool
{
    use io::{ErrorKind};

    match e.kind() {
        ErrorKind::WouldBlock => {  true  }
        _ => {  false  }
    }
}

#[derive(Clone,Debug)]
pub enum Ret {
    Wait,
    BadConnection
}

#[derive(Clone,Debug)]
pub enum Status {
    Ready,
    Connecting,
    // Closed,
    BadConnection
}

// F R S V   O   |  M    L     |            | 
// I S S S   P   |  A    E     |            | 
// N V V V       |  S    N     |            | 
//               |  K          |            | 
// --------------+-------------+------------+----------
// 1 0 0 0 0001  |  0 0010001  |  00000011  |  11110000

struct WSFrame {
    fin: bool,
    opcode: u8,
    data_len: usize
}

type TlsSocket = BufReader<native_tls::TlsStream<Socket>>;

// #[derive(Clone,Debug)]
enum Stage {
    // Error
    SE,
    // Initiate connection
    S0(SockAddr,Socket),
    S00(Socket),
    // Mid handshake
    S1(MidHandshakeTlsStream<Socket>),
    // Sending upgrade protocol
    S2(TlsSocket),
    // Upgrade answer
    S3(TlsSocket),
    // Done
    S4(TlsSocket),
}

struct Head {
    fin: bool,
    opcode: u8,
    data_len: usize,
    read_len: usize
}

pub struct WebSocket {
    host: String,
    path: String,
    // timeout: Option<Duration>,
    // time: Instant,
    // status: Status,
    stage: Stage,
    head: Option<Head>,
    data: Vec<u8>,
    temp: Vec<u8>
    // body: Option<Vec<u8>>
}

impl WebSocket
{
    pub fn connect(sock_addr: SocketAddr, host: &str, path: &str) -> Self
    {
        let sock_addr: SockAddr = sock_addr.into();
        let socket = Socket::new(sock_addr.domain(), 
            socket2::Type::STREAM, None).unwrap();
        socket.set_nonblocking(true).unwrap();
        let stage = Stage::S0(sock_addr, socket);

        Self {
            host: host.to_string(),
            path: path.to_string(),
            stage,
            head: None,
            data: Vec::with_capacity(1024),
            temp: Vec::with_capacity(64),
        }
    }
    pub fn status(&self) -> Status {
        match self.stage {
            Stage::SE => { Status::BadConnection }
            Stage::S0(_,_) => { Status::Connecting }
            Stage::S00(_) => { Status::Connecting }
            Stage::S1(_) => { Status::Connecting }
            Stage::S2(_) => { Status::Connecting }
            Stage::S3(_) => { Status::Connecting }
            Stage::S4(_) => { Status::Ready }
        }
    }

    pub fn read_data(&mut self) -> Result<&[u8],Ret>
    {
        self.update();
        let s = mem::replace(&mut self.stage, Stage::SE);

        match s {
            Stage::SE => {
                return Err(Ret::BadConnection);
            }
            Stage::S4(mut sock) => {
                // println!("{}", line!());
                let f = match self.next_frame(&mut sock) {
                    Ok(f) => { f }
                    Err(e) => { 
                        match e {
                            Ret::BadConnection => { self.stage = Stage::SE; }
                            _ => { self.stage = Stage::S4(sock); }
                        }
                        return Err(e);
                    }
                };
                let body = &self.data[0..f.data_len];
                match f.opcode {
                    0x00 |
                        // continuation
                    0x01 |
                        // text
                    0x02 => {
                        // binary
                        if f.fin {
                            self.stage = Stage::S4(sock);
                            return Ok(body);
                        }
                    }
                    0x08 => { 
                        // close
                        // dbg!(body);
                        if body.len() > 1 {
                            let code = u16::from_be_bytes([body[0], body[1]]);
                            eprintln!("closing: {}; {}", code, core::str::from_utf8(&body[2..]).unwrap());
                        }
                    }
                    0x09 => {
                        // ping
                        let txt = format!("{}", core::str::from_utf8(body).unwrap());
                        // println!("ping: {}", txt);
                        ws_send_pong(txt.as_bytes(), sock.get_mut()).unwrap();
                        // println!("pong: {}", txt);
                    }
                    0x0A => {
                        // pong
                        unimplemented!();
                    }
                    _ => {
                        unimplemented!();
                    }
                }
                self.stage = Stage::S4(sock);
            }
            stg => {
                self.stage = stg;
            }
        }
        Err(Ret::Wait)
    }

    #[allow(dead_code)]
    pub fn write_data(&mut self, buf: &[u8]) -> io::Result<usize>
    {
        use io::{Error, ErrorKind};

        self.update();
        let s = mem::replace(&mut self.stage, Stage::SE);
        match s {
            Stage::SE => {
                Err(Error::from(ErrorKind::Other))
            }
            Stage::S4(mut sock) => {
                match ws_send_data(buf, sock.get_mut()) {
                    Ok(len) => {
                        self.stage = Stage::S4(sock);
                        Ok(len)
                    }
                    Err(e) => {
                        if would_block(&e) {
                            self.stage = Stage::S4(sock);
                        }
                        else {
                            self.stage = Stage::SE;
                        }
                        Err(e)
                    }
                }
            }
            stage => {
                self.stage = stage;
                Err(Error::from(ErrorKind::WouldBlock))
            }
        }
    }

    pub fn write_txt(&mut self, txt: &str) -> io::Result<usize>
    {
        use io::{Error, ErrorKind};

        self.update();
        let s = mem::replace(&mut self.stage, Stage::SE);
        match s {
            Stage::SE => {
                Err(Error::from(ErrorKind::Other))
            }
            Stage::S4(mut sock) => {
                match ws_send_text(txt, sock.get_mut()) {
                    Ok(len) => {
                        self.stage = Stage::S4(sock);
                        Ok(len)
                    }
                    Err(e) => {
                        if would_block(&e) {
                            self.stage = Stage::S4(sock);
                        }
                        else {
                            self.stage = Stage::SE;
                        }
                        Err(e)
                    }
                }
            }
            stage => {
                self.stage = stage;
                Err(Error::from(ErrorKind::WouldBlock))
            }
        }
    }

    #[allow(dead_code)]
    pub fn close(&mut self) -> io::Result<usize>
    {
        use io::{Error, ErrorKind};

        self.update();
        let s = mem::replace(&mut self.stage, Stage::SE);
        match s {
            Stage::SE => {
                Err(Error::from(ErrorKind::Other))
            }
            Stage::S4(mut sock) => {
                match ws_send_close(sock.get_mut()) {
                    Ok(len) => {
                        self.stage = Stage::S4(sock);
                        Ok(len)
                    }
                    Err(e) => {
                        if would_block(&e) {
                            self.stage = Stage::S4(sock);
                        }
                        else {
                            self.stage = Stage::SE;
                        }
                        Err(e)
                    }
                }
            }
            stage => {
                self.stage = stage;
                Err(Error::from(ErrorKind::WouldBlock))
            }
        }
    }

    pub fn update(&mut self)  {
        let s = mem::replace(&mut self.stage, Stage::SE);

        match s {
            Stage::SE => { }
            Stage::S0(sock_addr,socket) => {
                // dbg!(line!());
                self.stage0(sock_addr,socket);
            }
            Stage::S00(socket) => {
                // dbg!(line!());
                self.stage00(socket);
            }
            Stage::S1(m) => {
                // dbg!(line!());
                self.stage1(m);
            }
            Stage::S2(sock) => {
                // dbg!(line!());
                self.stage2(sock);
            }
            Stage::S3(sock) => {
                // dbg!(line!());
                self.stage3(sock);
            }
            Stage::S4(sock) => {
                // dbg!(line!());
                self.stage = Stage::S4(sock);
            }
        }
    }

    fn stage0(&mut self, sock_addr: SockAddr,socket: Socket)
    {
        use native_tls::{HandshakeError};

        match socket.connect(&sock_addr) {
            Ok(()) => {
                let tls_conn = TlsConnector::new().unwrap();
                let ret = tls_conn.connect(&self.host, socket);
                match ret {
                    Ok(s) => {
                        self.stage = Stage::S2(BufReader::with_capacity(8182, s));
                    }
                    Err(HandshakeError::Failure(_err)) => {
                        eprintln!("error: TLS handshake failure");
                        self.stage = Stage::SE;
                    }
                    Err(HandshakeError::WouldBlock(m)) => {
                        self.stage = Stage::S1(m);
                    }
                }
            }
            Err(e) => {
                match e.kind() {
                    std::io::ErrorKind::WouldBlock => {
                        self.stage = Stage::S00(socket);
                        return;
                    }
                    _ => {}
                }
                match e.raw_os_error() {
                    // Some(libc::EALREADY) | 
                    Some(libc::EINPROGRESS) => {
                        self.stage = Stage::S00(socket);
                    }
                    _ => {
                        use std::error::Error;
                        dbg!(e.kind());
                        dbg!(e.source());
                        dbg!(e.raw_os_error());
                        dbg!(e);
                        self.stage = Stage::SE;
                    }
                }
            }
        }
    }

    fn stage00(&mut self, socket: Socket)
    {
        use std::os::fd::AsRawFd;
        use libc::{POLLIN,POLLOUT,POLLHUP,POLLERR};
        use libc::poll;
        use native_tls::HandshakeError;

        let mut pollfd = libc::pollfd {
            fd: socket.as_raw_fd(),
            events: POLLIN | POLLOUT,
            revents: 0,
        };
        let timeout = 0;
        let res = unsafe { poll(&mut pollfd, 1, timeout) };
        match res {
            -1 => {
                // error
                let err = io::Error::last_os_error();
                if err.kind() == io::ErrorKind::Interrupted {
                    self.stage = Stage::S00(socket);
                }
                else {
                    self.stage = Stage::SE;
                }
            }
            0 => {
                // panic!("timeout event for nonblocking socket");
                self.stage = Stage::S00(socket);
            }
            1 => {
                if (pollfd.revents & POLLHUP) != 0 || (pollfd.revents & POLLERR) != 0
                {
                    self.stage = Stage::SE;
                }
                else {
                    let tls_conn = TlsConnector::new().unwrap();
                    let ret = tls_conn.connect(&self.host, socket);
                    match ret {
                        Ok(s) => {
                            self.stage = Stage::S2(BufReader::with_capacity(8182, s));
                        }
                        Err(HandshakeError::Failure(_err)) => {
                            // dbg!(err);
                            self.stage = Stage::SE;
                        }
                        Err(HandshakeError::WouldBlock(m)) => {
                            self.stage = Stage::S1(m);
                        }
                    }
                }
            }
            _ => {
                panic!("unexpected result");
            }
        }
    }

    fn stage1(&mut self, m: MidHandshakeTlsStream<Socket>)
    {
        use native_tls::HandshakeError;

        let ret = m.handshake();
        match ret {
            Ok(s) => {
                self.stage = Stage::S2(BufReader::with_capacity(4096, s));
            }
            Err(HandshakeError::Failure(_err)) => {
                eprintln!("error: TLS handshake failure(mid)");
                self.stage = Stage::SE;
            }
            Err(HandshakeError::WouldBlock(m)) => {
                self.stage = Stage::S1(m);
            }
        }
    }

    fn stage2(&mut self, mut sock: TlsSocket)
    {
        match ws_hs_header(&self.host, &self.path, "VfrYrKo3i0cz/Yxxpnm9Mg==", sock.get_mut()) {
            Ok(_) => {
                self.stage = Stage::S3(sock);
            }
            Err(_e) => {
                eprintln!("{}:{}: sending WS handshake", file!(), line!());
                self.stage = Stage::SE;
            }
        }
    }

    fn stage3(&mut self, mut sock: TlsSocket)
    {
        use io::{Read};

        let mut buf = [0u8; 512];
        loop { match sock.read(&mut buf) {
            Ok(len) => {
                let dat = &mut self.data;
                for b in &buf[0..len] {
                    // dbg!();
                    dat.push(*b);
                    if dat.ends_with(b"\r\n\r\n") {
                        if sub_slice(dat, b"HTTP/1.1 101").is_none() || 
                            sub_slice(dat, b"8GXekgtuByDZ+B+Qp12bb8yVhic=").is_none() {
                            eprintln!("error: received wrong HTTP response");
                            self.stage = Stage::SE;
                            return;
                        }
                        // println!("{}", core::str::from_utf8(&dat).unwrap());
                        dat.clear();
                        self.stage = Stage::S4(sock);
                        return;
                    }
                }
                // println!("{}", core::str::from_utf8(&dat).unwrap());
                // self.stage = Stage::S3(sock);
            }
            Err(e) => {
                if would_block(&e) {
                    self.stage = Stage::S3(sock);
                }
                else {
                    eprintln!("error: reading HTTP response");
                    self.stage = Stage::SE;
                }
                break;
            }
        } }
    }

    fn next_frame(&mut self, sock: &mut TlsSocket) -> Result<WSFrame,Ret>
    {
        use std::io::Read;

        let mut buf = [0u8; 2];

        if let Some(ref mut h) = self.head {
            for _ in 0..4 {
                if h.data_len == h.read_len {
                    let f = WSFrame { fin: h.fin, opcode: h.opcode, data_len: h.data_len };
                    self.head = None;
                    // println!("");
                    return Ok(f);
                }
                match sock.read(&mut self.data[h.read_len..h.data_len]) {
                    Ok(0) => { break; }
                    Ok(l) => {
                        // for b in &self.data[h.read_len..(h.read_len+l)] {
                            // print!("{:x} ", b);
                        // }
                        h.read_len += l;
                    }
                    Err(e) => {
                        if !would_block(&e) {
                            eprintln!("error: reading frame");
                            return Err(Ret::BadConnection);
                        }
                        break;
                    }
                }
            }
        }
        else {
            for _ in 0..4 {
                match sock.read(&mut buf[0..1]) {
                    Ok(0) => { break; }
                    Ok(_l) => {
                        // print!("{:x} ", buf[0]);
                        // println!("");
                        self.temp.push(buf[0]);
                        self.recog_head_();
                        if self.head.is_some() { break; }
                    }
                    Err(e) => {
                        if !would_block(&e) {
                            eprintln!("error: reading head of frame");
                            return Err(Ret::BadConnection);
                        }
                        break;
                    }
                }
            }
        }
        Err(Ret::Wait)
    }

    fn recog_head_(&mut self)
    {
        let h_len = self.temp.len();
        let dat = self.temp.as_slice();
        let mut need_len = 2;
        if h_len < need_len { return; }
        let b = dat[0];
        let fin = (b & 0x80) == 0x80;
        let opcode = b & 0x0F;
        let b = dat[1];
        let mask = (b & 0x80) == 0x80;
        let data_len: usize = match b & 0x7F {
            l @ 0x00..=0x7D => { l.into() }
            0x7E => {
                need_len += 2;
                if h_len < need_len { return; }
                u16::from_be_bytes([dat[2], dat[3]]) as usize
            }
            0x7F => {
                need_len += 4;
                if h_len < need_len { return; }
                u32::from_be_bytes(
                    [dat[2], dat[3], dat[4], dat[5]]) as usize
            }
            _ => { panic!("unexpected value of length"); }
        };
        if mask {
            need_len += 4;
            if h_len < need_len { return; }
            println!("WS Frame is masked");
        }
        let read_len = 0;
        self.temp.clear();
        self.data.resize(data_len, 0);
        self.head = Some(Head { fin, opcode, data_len, read_len });
    }
}

impl Drop for WebSocket {
    fn drop(&mut self) {
        // match self.close() {
        // }
    }
}

// handshake header for Websocket
fn ws_hs_header<O: io::Write>(host: &str, path: &str, key: &str, out: &mut O) -> io::Result<()>
{
    write!(out, "GET /{} HTTP/1.1\r\nHost: {}:443\r\n", path, host)?;
    write!(out, "Connection: Upgrade\r\n")?;
    write!(out, "Upgrade: websocket\r\n")?;
    // write!(out, "Origin: https://{}\r\n", host)?;
    write!(out, "Sec-WebSocket-Version: 13\r\n")?;
    write!(out, "Sec-WebSocket-Key: {}\r\n\r\n", key)?;
    Ok(())
}

fn ws_send_data<O: io::Write>(data: &[u8], out: &mut O) -> io::Result<usize>
{
    use io::{Error, ErrorKind};

    let len = data.len();
    match len {
        0..=0x7D => {
            let t = [0x82, len as u8];
            out.write(&t)?;
        }
        0x7E..=0xFFFF => {
            let bts = (len as u16).to_le_bytes();
            let t = [0x82, 0x7E, bts[0], bts[1]];
            out.write(&t)?;
        }
        0x10000..=usize::MAX => { /*0x82, 0x00]*/ unimplemented!(); }
        _ => { return Err(Error::from(ErrorKind::Other)); }
    };
    out.write(data)
}


fn ws_send_text<O: io::Write>(txt: &str, out: &mut O) -> io::Result<usize>
{
    // use std::io::Write;
    use io::{Error, ErrorKind};

    let mask = [0xAA, 0xBB, 0xCC, 0xDD];
    // let mask = (timestamp() as u32).to_be_bytes();
    let bytes = txt.as_bytes();
    let len = bytes.len();
    let bit_mask = 0x80;
    match len {
        0..=0x7D => {
            let t = [0x81, len as u8 | bit_mask];
            out.write(&t)?;
            out.write(&mask)?;
        }
        0x7E..=0xFFFF => {
            let bts = (len as u16).to_be_bytes();
            let t = [0x81, 0x7E | bit_mask, bts[0], bts[1]];
            out.write(&t)?;
            out.write(&mask)?;
        }
        0x10000..=usize::MAX => { /*0x81, 0x00]*/ unimplemented!(); }
        _ => { return Err(Error::from(ErrorKind::Other)); }
    };
    {
        let mut temp_buf = [0u8; 1024];
        let mut pos = 0;
        while pos < len {
            let ll = (len - pos).min(1024);
            for i in 0..ll {
                let ii = i & 0x00003;
                temp_buf[i] = bytes[pos + i] ^ mask[ii];
            }
            pos += ll;
            out.write(&temp_buf[0..ll])?;
        }
        return Ok(pos);
    }
    // {
        // out.write(bytes)
    // }
}

fn ws_send_pong<O: io::Write>(data: &[u8], out: &mut O) -> io::Result<usize>
{
    use io::{Error, ErrorKind};

    let len = data.len();
    let t = match len {
        0..=0x7D => { [0x8A, len as u8] }
        0x7E..=0xFFFF => { /*[0x81, 0x7E]*/ unimplemented!(); }
        0x10000..=usize::MAX => { /*0x81, 0x00]*/ unimplemented!(); }
        _ => { return Err(Error::from(ErrorKind::Other)); }
    };
    out.write(&t)?;
    out.write(data)
}

fn ws_send_close<O: io::Write>(out: &mut O) -> io::Result<usize>
{
    out.write(&[0x88, 0x82, 0x00, 0x00, 0x00, 0x00, 0x03, 0xea])
}
