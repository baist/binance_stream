use std::net::SocketAddr;
use std::collections::VecDeque;

use crate::timestamp;
use crate::web_socket::Status as WSStatus;
use crate::web_socket::Ret as WSRet;
use crate::web_socket::WebSocket;

use binance_api::user_stream_resp;
use user_stream_resp::{AccountUpdate, OrderUpdate};

pub struct UserStream {
    sock_addr: SocketAddr,
    list_key: String,
    latency: u64,
    ws_stream: Option<WebSocket>,
    acc_updates: VecDeque<AccountUpdate>,
    order_updates: VecDeque<OrderUpdate>
}

impl UserStream {
    pub fn new(sock_addr: SocketAddr, list_key: &str) -> Self
    {
        Self {
            sock_addr,
            list_key: list_key.to_string(),
            latency: 0,
            ws_stream: None,
            acc_updates: VecDeque::with_capacity(16),
            order_updates: VecDeque::with_capacity(16)
        }
    }

    pub fn latency(&self) -> u64
    {
        self.latency
    }

    pub fn connected(&mut self) -> bool
    {
        if self.ws_stream.is_none() { return false; }
        let ws_stream = self.ws_stream.as_mut().unwrap();
        ws_stream.update();
        match ws_stream.status() {
            WSStatus::Ready => { return true; }
            _ => {}
        }
        false
    }

    pub fn connect(&mut self)
    {
        if self.ws_stream.is_none() {
            let path = format!("ws/{}", self.list_key.as_str());
            self.ws_stream = Some(WebSocket::connect(
                self.sock_addr.clone(), "stream.binance.com", &path));
        }
    }

    pub fn update(&mut self)
    {
        if self.ws_stream.is_none() {
            let path = format!("ws/{}", self.list_key.as_str());
            self.ws_stream = Some(WebSocket::connect(
                self.sock_addr.clone(), "stream.binance.com", &path));
        }
        let stream = if let Some(s) = self.ws_stream.as_mut() { s }
            else { return };
        stream.update();
        match stream.status() {
            WSStatus::Ready => { }
            _ => { return; }
        }
        match stream.read_data() {
            Ok(v) => {
                // println!("{}", core::str::from_utf8(&v).unwrap());
                if v.is_empty() { return; }
                //
                let tp = match user_stream_resp::recog_type(&v) {
                    Ok(a) => { a }
                    Err(_e) => {
                        eprintln!("{}:{}: unsupport type", file!(), line!());
                        return;
                    } 
                };
                match tp.as_str() {
                    "outboundAccountPosition" => {
                        match user_stream_resp::account_update(&v) {
                            Ok(acc_upd) => {
                                let la = timestamp() as i64 - acc_upd.event_time as i64;
                                self.latency = if la > 0 { la as u64 } else { 0 };
                                self.acc_updates.push_back(acc_upd);
                            }
                            Err(code) => {
                                eprintln!("error: {}: wrong data", code);
                                return;
                            }
                        }
                    }
                    "executionReport" => {
                        match user_stream_resp::order_update(&v) {
                            Ok(order_upd) => {
                                let la = timestamp() as i64 - order_upd.event_time as i64;
                                self.latency = if la > 0 { la as u64 } else { 0 };
                                self.order_updates.push_back(order_upd);
                            }
                            Err(code) => {
                                eprintln!("error: {}: wrong data", code);
                                return;
                            }
                        }
                    }
                    _ => { return; /* Err(line!())*/ }
                }
            }
            Err(WSRet::Wait) => { }
            Err(WSRet::BadConnection) => {
                eprintln!("{}:{}: bad connection", file!(), line!());
                return 
            }
        }
    }

    pub fn take_acc_updates(&mut self, acc_updates: &mut[AccountUpdate]) -> Result<usize, u8>
    {
        self.update();
        let count = acc_updates.len();
        let mut i = 0;
        while i < count {
            if let Some(cndl) = self.acc_updates.pop_front() {
                acc_updates[i] = cndl;
                i += 1;
            }
            else { break; }
        }
        Ok(i)
    }

    pub fn take_ordr_updates(&mut self, order_updates: &mut[OrderUpdate]) -> Result<usize, u8>
    {
        self.update();
        let count = order_updates.len();
        let mut i = 0;
        while i < count {
            if let Some(cndl) = self.order_updates.pop_front() {
                order_updates[i] = cndl;
                i += 1;
            }
            else { break; }
        }
        Ok(i)
    }

    pub fn take_acc_update(&mut self) -> Option<AccountUpdate>
    {
        self.update();
        self.acc_updates.pop_front()
    }

    pub fn take_ordr_update(&mut self) -> Option<OrderUpdate>
    {
        self.update();
        self.order_updates.pop_front()
    }
}