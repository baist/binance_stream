use core::sync::atomic::AtomicBool;
use std::collections::VecDeque;
use std::sync::Arc;
use std::net::SocketAddr;
use binance_api::types::{Trade,Interval};
use binance_api::{ws_stream_req,ws_stream_resp};

use ws_stream_resp::{Kline};

mod web_socket;
mod user_stream;

pub use user_stream::UserStream;

use web_socket::Status as WSStatus;
use web_socket::WebSocket;

fn timestamp() -> u64
{
    use std::time::{SystemTime, UNIX_EPOCH};

    let ts = 
        SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_millis();
    assert!(ts < (u64::MAX as _));
    ts as u64
}

fn check_trades_order(trade: &Trade)
{
    static mut LAST_TS: u64 = 0;
    unsafe {
        let temp_ts = LAST_TS;
        if trade.ts < temp_ts {
            panic!("bad order of trades: {} < {}", trade.ts, temp_ts);
        }
        else { LAST_TS = trade.ts; }
    }
}

// fn log(line: u32)
// {
//     static mut repeat: bool = false;
//     static mut prev_line: u32 = 0;
//     unsafe {
//         if prev_line == line {
//             if !repeat {
//                 println!("  ...");
//                 repeat = true;
//             }
//         }
//         else {
//             println!("{}: {}", file!(), line);
//             prev_line = line;
//             repeat = false;
//         }
//     }
// }

enum ReqType {
    Trade,
    Kline(Interval)
}

enum ReqMethod {
    Subscribe,
    Unsubscribe,
}

struct ReqParam {
    symbol: String,
    req_type: ReqType
}

struct Request {
    method: ReqMethod,
    id: u64,
    param: ReqParam,
    //
    active: bool
}

pub struct MarketStream {
    sock_addr: SocketAddr,
    latency: u64,
    last_id: u64,
    flag: Arc<AtomicBool>,
    ws_stream: Option<WebSocket>,
    requests: VecDeque<Request>,
    //
    trades: VecDeque<Trade>,
    klines: VecDeque<Kline>,
}

impl MarketStream
{
    pub fn new(sock_addr: SocketAddr, _symb: &str) -> Self
    {
        Self {
            sock_addr,
            latency: 0,
            last_id: 0,
            flag: Arc::new(AtomicBool::new(false)),
            ws_stream: None,
            requests: VecDeque::with_capacity(16),
            trades: VecDeque::with_capacity(16),
            klines: VecDeque::with_capacity(16)
        }
    }

    pub fn latency(&self) -> u64
    {
        self.latency
    }

    pub fn connect(&mut self)
    {
        // log(line!());
        self.try_to_connect();
    }

    pub fn sub_trade(&mut self, symb: &str)
    {
            // log(line!());
            // log(line!());
        self.last_id += 1;
        let req = Request {
            method: ReqMethod::Subscribe,
            id: self.last_id,
            param: ReqParam {
                symbol: symb.to_string(), req_type: ReqType::Trade },
            active: false
        };
        self.add_request(req);
    }

    pub fn unsub_trade(&mut self, symb: &str)
    {
        self.last_id += 1;
        let req = Request {
            method: ReqMethod::Unsubscribe,
            id: self.last_id,
            param: ReqParam {
                symbol: symb.to_string(), req_type: ReqType::Trade },
            active: false
        };
        self.add_request(req);
    }

    pub fn sub_kline(&mut self, symb: &str, intrvl: Interval)
    {
        self.last_id += 1;
        let req = Request {
            method: ReqMethod::Subscribe,
            id: self.last_id,
            param: ReqParam {
                symbol: symb.to_string(), req_type: ReqType::Kline(intrvl) },
            active: false
        };
        self.add_request(req);
    }

    pub fn unsub_kline(&mut self, symb: &str, intrvl: Interval)
    {
        self.last_id += 1;
        let req = Request {
            method: ReqMethod::Unsubscribe,
            id: self.last_id,
            param: ReqParam {
                symbol: symb.to_string(), req_type: ReqType::Kline(intrvl) },
            active: false
        };
        self.add_request(req);
    }

    pub fn take_klines(&mut self, klines: &mut[Kline]) -> Result<usize, u8>
    {
        self.update();
        let count = klines.len();
        let mut i = 0;
        while i < count {
            if let Some(cndl) = self.klines.pop_front() {
                klines[i] = cndl;
                i += 1;
            }
            else { break; }
        }
            // log(line!());
        Ok(i)
    }

    pub fn take_trades(&mut self, trades: &mut[Trade]) -> Result<usize, u8>
    {
        self.update();
        let count = trades.len();
        let mut i = 0;
        while i < count {
            if let Some(cndl) = self.trades.pop_front() {
                trades[i] = cndl;
                i += 1;
            }
            else { break; }
        }
        Ok(i)
    }

    pub fn take_kline(&mut self) -> Option<Kline>
    {
        self.update();
        self.klines.pop_front()
    }

            // log(line!());
    pub fn take_trade(&mut self) -> Option<Trade>
    {
        self.update();
        self.trades.pop_front()
    }

    fn add_request(&mut self, req: Request)
    {
        self.requests.push_back(req);
    }

    fn update(&mut self)
    {
        use web_socket::{Ret};

        let stream = if let Some(s) = self.ws_stream.as_mut() { s } 
            else { return };

        stream.update();
        match stream.status() {
            WSStatus::Ready => { }
            _ => { return; }
        }
        
        for _ in 0..self.requests.len() {
            let mut req = self.requests.pop_front().unwrap();
            if !req.active {
                if Self::do_req(stream, &req) {
                    req.active = true;
                    self.requests.push_back(req);
                }
                else {
                    eprintln!("something wrong with request {}", req.id);
                }
            }
            else { self.requests.push_back(req); }
        }


                        // log(line!());
        match stream.read_data() {
            Ok(v) => {
                // println!("{}", core::str::from_utf8(&v).unwrap());
                if v.is_empty() { return; }
                //
                for _ in 0..self.requests.len() {
                    let req = self.requests.pop_front().unwrap();
                    if req.active {
                        match ws_stream_resp::ret_result(&v) {
                            Ok(id) => {
                                if id == req.id { return; }
                            }
                            _ => { }
                        }
                    }
                        // log(line!());
                                // println!("{}", core::str::from_utf8(&v).unwrap());
                    self.requests.push_back(req);
                }
                //
                let tp = match ws_stream_resp::recog_type(&v) {
                    Ok(a) => { a }
                    Err(_e) => {
                        eprintln!("{}:{}: unsupport type", file!(), line!());
                        return;
                    } 
                };
                match tp.as_str() {
                    "trade" => {
                       match ws_stream_resp::trade(&v) {
                            Ok(trade) => {
                                let la = timestamp() as i64 - trade.ts as i64;
                                self.latency = if la > 0 { la as u64 } else { 0 };
                                self.trades.push_back(trade);
                            }
                            Err(code) => {
                                eprintln!("error: {}: wrong trade data in WS stream\n{}", 
                                    code, core::str::from_utf8(&v).unwrap());
                                return;
                            }
                        } 
                    }
                    "kline" => {
                        match ws_stream_resp::kline(&v) {
                            Ok(kline) => {
                                let la = timestamp() as i64 - kline.event_ts as i64;
                                self.latency = if la > 0 { la as u64 } else { 0 };
                                self.klines.push_back(kline);
                            }
                            Err(code) => {
                                eprintln!("error: {}: wrong kline data in WS stream\n{}", 
                                    code, core::str::from_utf8(&v).unwrap());
                                return;
                            }
                        }
                    }
                    _ => { return; /* Err(line!())*/ }
                }
            }
            Err(Ret::Wait) => { }
            Err(Ret::BadConnection) => {
                eprintln!("bad connection");
                return 
            }
        }
    }

    fn try_to_connect(&mut self)
    {
        if self.ws_stream.is_none() {
            self.ws_stream = Some(WebSocket::connect(
                self.sock_addr.clone(), "stream.binance.com", "ws"));
        }
    }

    fn do_req(ws_stream: &mut WebSocket, req: &Request) -> bool
    {
        let mut msg = String::with_capacity(64);
        let v = unsafe { msg.as_mut_vec() };
        match req.method {
            ReqMethod::Subscribe => {
                match &req.param.req_type {
                    ReqType::Trade => {
                        ws_stream_req::sub_trade(req.id as usize,
                            &req.param.symbol, v).unwrap();
                    }
                    ReqType::Kline(interv) => {
                        ws_stream_req::sub_kline(req.id as usize, 
                            &req.param.symbol, interv.clone(), v).unwrap();
                    }
                }
            }
            ReqMethod::Unsubscribe => {
                match &req.param.req_type {
                    ReqType::Trade => {
                        ws_stream_req::unsub_trade(req.id as usize,
                            &req.param.symbol, v).unwrap();
                    }
                    ReqType::Kline(interv) => {
                        ws_stream_req::unsub_kline(req.id as usize, 
                            &req.param.symbol, interv.clone(), v).unwrap();
                    }
                }
            }
        }
        ws_stream.write_txt(&msg).unwrap();
        true
    }
}


impl Drop for MarketStream {
    fn drop(&mut self) {
        use core::sync::atomic::Ordering;

        self.flag.store(false, Ordering::Relaxed);
    }
}
